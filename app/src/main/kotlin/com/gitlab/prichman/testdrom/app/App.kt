package com.gitlab.prichman.testdrom.app

import androidx.multidex.MultiDexApplication

import com.gitlab.prichman.testdrom.BuildConfig
import com.gitlab.prichman.testdrom.app.di.ServiceLocator
import com.gitlab.prichman.testdrom.presentation.FailureFactory
import com.gitlab.prichman.testdrom.presentation.ResourceManagerImpl
import com.gitlab.prichman.testdrom.presentation.viewmodel.ViewModelsFactory


class App : MultiDexApplication() {

    companion object {
        lateinit var app: App
    }

    val viewModelsFactory by lazy {
        ViewModelsFactory(
            serviceLocator.instanceProvider.provideSearchRepositoriesUseCase(),
            serviceLocator.instanceProvider.provideGetIssuesForRepoUseCase()
        )
    }

    private val failureFactory by lazy { FailureFactory(ResourceManagerImpl(this)) }
    private val serviceLocator by lazy { ServiceLocator(BuildConfig.BUILD_T, failureFactory) }

    override fun onCreate() {
        super.onCreate()

        app = this
    }
}