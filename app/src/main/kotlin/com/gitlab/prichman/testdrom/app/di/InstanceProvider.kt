package com.gitlab.prichman.testdrom.app.di

import com.gitlab.prichman.testdrom.app.di.usecase.*

interface InstanceProvider : SearchRepositoriesUseCaseProvider,
        GetIssuesForRepoUseCaseProvider