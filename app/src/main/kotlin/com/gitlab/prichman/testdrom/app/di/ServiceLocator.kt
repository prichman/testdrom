package com.gitlab.prichman.testdrom.app.di

import com.gitlab.prichman.testdrom.data.api.GithubApi
import com.gitlab.prichman.testdrom.presentation.FailureFactory

class ServiceLocator(
    configuration: Int = TEST_CONFIG,
    failureFactory: FailureFactory
) {

    companion object {
        const val PRODUCTION_CONFIG = 1
        const val TEST_CONFIG       = 2

        private const val TOKEN = ""
    }

    val instanceProvider: InstanceProvider by lazy {
        when (configuration) {
            PRODUCTION_CONFIG -> BaseInstanceProvider(
                githubApi = GithubApi.create(token = TOKEN),
                failureFactory = failureFactory
            )
            TEST_CONFIG -> TestInstanceProvider(
                failureFactory = failureFactory
            )
            else -> throw IllegalArgumentException("Incorrect type of configuration given")
        }
    }
}