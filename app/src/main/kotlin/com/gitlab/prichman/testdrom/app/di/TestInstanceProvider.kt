package com.gitlab.prichman.testdrom.app.di

import com.gitlab.prichman.testdrom.data.datasource.TestGithubDataSource
import com.gitlab.prichman.testdrom.data.repository.GithubRepoImpl
import com.gitlab.prichman.testdrom.domain.usecase.GetIssuesForRepoUseCase
import com.gitlab.prichman.testdrom.domain.usecase.SearchRepositoriesUseCase
import com.gitlab.prichman.testdrom.domain.usecase.impl.GetIssuesForRepoUseCaseImpl
import com.gitlab.prichman.testdrom.domain.usecase.impl.SearchRepositoriesUseCaseImpl
import com.gitlab.prichman.testdrom.presentation.FailureFactory


class TestInstanceProvider(
    private val failureFactory: FailureFactory
) : InstanceProvider {

    private val repo = GithubRepoImpl(TestGithubDataSource())

    override fun provideSearchRepositoriesUseCase(): SearchRepositoriesUseCase =
        SearchRepositoriesUseCaseImpl(repo, failureFactory)

    override fun provideGetIssuesForRepoUseCase(): GetIssuesForRepoUseCase =
        GetIssuesForRepoUseCaseImpl(repo, failureFactory)
}