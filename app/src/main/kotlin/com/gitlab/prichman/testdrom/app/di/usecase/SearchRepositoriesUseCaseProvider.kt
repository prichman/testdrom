package com.gitlab.prichman.testdrom.app.di.usecase

import com.gitlab.prichman.testdrom.domain.usecase.SearchRepositoriesUseCase

interface SearchRepositoriesUseCaseProvider {
    fun provideSearchRepositoriesUseCase(): SearchRepositoriesUseCase
}