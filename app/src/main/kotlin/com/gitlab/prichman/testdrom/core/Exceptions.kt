package com.gitlab.prichman.testdrom.core

import java.io.IOException


class NoConnectionException : IOException {
    constructor() : super()
    constructor(cause: Exception) : super(cause)
}

class NetworkException : IOException {
    constructor() : super()
    constructor(cause: Exception) : super(cause)
}

class RequestQuotaExceedException : IOException {
    constructor() : super()
    constructor(cause: Exception) : super(cause)
}