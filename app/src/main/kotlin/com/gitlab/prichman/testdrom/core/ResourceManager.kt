package com.gitlab.prichman.testdrom.core

import androidx.annotation.StringRes

interface ResourceManager {
    fun getString(@StringRes stringResId: Int): String
}