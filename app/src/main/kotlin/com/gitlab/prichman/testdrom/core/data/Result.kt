package com.gitlab.prichman.testdrom.core.data

sealed class Result<out T> {
    class Success<out R>(val value: R) : Result<R>()
    class Failure(val error: Exception) : Result<Nothing>()
}