package com.gitlab.prichman.testdrom.core.domain

interface FailureReason {
    val message: String
}

interface ExceptionHandler {
    fun handle(e: Exception): FailureReason
}