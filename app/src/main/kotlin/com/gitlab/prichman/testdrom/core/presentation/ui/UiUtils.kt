package com.gitlab.prichman.testdrom.core.presentation.ui

import android.widget.ImageView

import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade

import com.gitlab.prichman.testdrom.R


object UiUtils {

    fun loadGlideAvatar(
        glide: RequestManager,
        url: String,
        imageView: ImageView
    ) {
        glide.asDrawable()
            .placeholder(R.drawable.ic_account_circle_24)
            .transition(withCrossFade())
            .load(url)
            .circleCrop()
            .into(imageView)

    }
}