package com.gitlab.prichman.testdrom.data.api

import com.gitlab.prichman.testdrom.data.vo.GithubIssueDTO

import okhttp3.Dispatcher
import okhttp3.Interceptor
import okhttp3.OkHttpClient

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface GithubApi {

    companion object {
        private const val BASE_URL = "https://api.github.com"

        fun create(token: String = ""): GithubApi {

            val dispatcher = Dispatcher()
            dispatcher.maxRequests = 1

            val timeInterceptor = Interceptor { chain ->
                try {
                    Thread.sleep(200)
                } catch (e: Throwable) {
                    // Nothing to do
                }
                chain.proceed(chain.request())
            }

            val authInterceptor = Interceptor { chain ->
                val requestBuilder = chain.request()
                    .newBuilder()
                    .addHeader("Accept", "application/vnd.github.v3+json")
                    .apply {
                        if (token.isNotBlank()) {
                            addHeader("Authorization", "token $token")
                        }
                    }

                chain.proceed(requestBuilder.build())
            }

            val client = OkHttpClient.Builder()
                .addNetworkInterceptor(timeInterceptor)
                .addNetworkInterceptor(authInterceptor)
                .dispatcher(dispatcher)
                .build()

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(GithubApi::class.java)
        }
    }

    @GET("/search/repositories")
    suspend fun searchRepositories(
        @Query("q") query: String,
        @Query("per_page") perPage: Int,
        @Query("page") page: Int
    ): SearchReposResponse

    @GET("/repos/{owner}/{repo}/issues")
    suspend fun getIssues(
        @Path("owner") ownerName: String,
        @Path("repo") repoName: String,
        @Query("per_page") perPage: Int,
        @Query("page") page: Int
    ): List<GithubIssueDTO>
}