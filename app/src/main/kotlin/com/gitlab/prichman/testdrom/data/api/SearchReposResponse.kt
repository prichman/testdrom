package com.gitlab.prichman.testdrom.data.api

import com.gitlab.prichman.testdrom.data.vo.GithubRepositoryDTO

data class SearchReposResponse(val items: List<GithubRepositoryDTO>)