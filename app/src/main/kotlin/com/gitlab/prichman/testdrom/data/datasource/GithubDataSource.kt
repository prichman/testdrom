package com.gitlab.prichman.testdrom.data.datasource

import com.gitlab.prichman.testdrom.core.NetworkException
import com.gitlab.prichman.testdrom.core.NoConnectionException
import com.gitlab.prichman.testdrom.core.RequestQuotaExceedException
import com.gitlab.prichman.testdrom.core.data.Result
import com.gitlab.prichman.testdrom.data.api.GithubApi
import com.gitlab.prichman.testdrom.data.vo.GithubIssueDTO
import com.gitlab.prichman.testdrom.data.vo.GithubRepositoryDTO

import retrofit2.HttpException

import java.io.IOException
import java.net.UnknownHostException

class GithubDataSource(
    private val githubApi: GithubApi
) : NetworkDataSource {

    private companion object {
        private const val QUOTA_ERROR_CODE = 403
    }

    @Suppress("UNCHECKED_CAST")
    override suspend fun searchRepositories(
        query: String,
        perPage: Int,
        page: Int
    ): Result<List<GithubRepositoryDTO>> = handleErrors {
        val searchResult = githubApi.searchRepositories(query = query, perPage = perPage, page = page)

        Result.Success(searchResult.items)
    } as Result<List<GithubRepositoryDTO>>

    @Suppress("UNCHECKED_CAST")
    override suspend fun getIssues(
        owner: String,
        repo: String,
        perPage: Int,
        page: Int
    ): Result<List<GithubIssueDTO>> = handleErrors {
        Result.Success(
            value = githubApi.getIssues(
                ownerName = owner,
                repoName = repo,
                perPage = perPage,
                page = page
            )
        )
    } as Result<List<GithubIssueDTO>>


    private suspend fun handleErrors(tryBlock: suspend () -> Result.Success<List<*>>): Result<List<*>> {
        return try {
            tryBlock()
        } catch (e: IOException) {
            when (e) {
                is UnknownHostException -> Result.Failure(NoConnectionException(e))
                else -> Result.Failure(NetworkException(e))
            }
        } catch (e: HttpException) {
            if (e.code() == QUOTA_ERROR_CODE) {
                Result.Failure(RequestQuotaExceedException(e))
            } else {
                Result.Failure(NetworkException(e))
            }
        }
        catch (e: Exception) {
            Result.Failure(e)
        }
    }
}