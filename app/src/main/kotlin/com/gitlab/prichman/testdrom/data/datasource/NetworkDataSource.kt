package com.gitlab.prichman.testdrom.data.datasource

import com.gitlab.prichman.testdrom.core.data.Result
import com.gitlab.prichman.testdrom.data.vo.GithubIssueDTO
import com.gitlab.prichman.testdrom.data.vo.GithubRepositoryDTO

interface NetworkDataSource {
    suspend fun searchRepositories(
        query: String,
        perPage: Int,
        page: Int
    ): Result<List<GithubRepositoryDTO>>

    suspend fun getIssues(
        owner: String,
        repo: String,
        perPage: Int,
        page: Int
    ): Result<List<GithubIssueDTO>>
}