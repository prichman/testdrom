package com.gitlab.prichman.testdrom.data.datasource

import com.gitlab.prichman.testdrom.core.data.Result
import com.gitlab.prichman.testdrom.data.vo.GithubIssueDTO
import com.gitlab.prichman.testdrom.data.vo.GithubOwnerDTO
import com.gitlab.prichman.testdrom.data.vo.GithubRepositoryDTO

import kotlinx.coroutines.delay

class TestGithubDataSource : NetworkDataSource {

    private companion object {
        private const val PAGE_SIZE = 12
        private const val LOREM = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        private const val DELAY = 50 * 100L
    }

    override suspend fun searchRepositories(
        query: String,
        perPage: Int,
        page: Int
    ): Result<List<GithubRepositoryDTO>> {
        delay(DELAY)

//        return getSuccessEmptyResult()
        return getSuccessRepos(page)
//        return getFailureResult()
    }

    private fun getSuccessRepos(page: Int): Result<List<GithubRepositoryDTO>> {
        val result = mutableListOf<GithubRepositoryDTO>()
        val startVal = (page - 1) * PAGE_SIZE + 1
        val endVal = startVal + PAGE_SIZE
        for (i in startVal until endVal) {
            result.add(generateTestRepo(i))
        }

        return Result.Success(result)
    }

    private fun getSuccessEmptyRepos(): Result<List<GithubRepositoryDTO>> {
        return Result.Success(emptyList())
    }

    private fun getFailureRepos(): Result<List<GithubRepositoryDTO>> {
        return Result.Failure(IllegalStateException("KU-KA"))
    }

    override suspend fun getIssues(
        owner: String,
        repo: String,
        perPage: Int,
        page: Int
    ): Result<List<GithubIssueDTO>> {
        delay(DELAY)

//        return getSuccessEmptyIssues()
        return getSuccessIssues(page)
//        return getFailureIssues()
    }

    private fun getSuccessIssues(page: Int): Result<List<GithubIssueDTO>> {
        val result = mutableListOf<GithubIssueDTO>()
        val startVal = (page - 1) * PAGE_SIZE + 1
        val endVal = startVal + PAGE_SIZE
        for (i in startVal until endVal) {
            result.add(generateTestIssue(i))
        }

        return Result.Success(result)
    }

    private fun getSuccessEmptyIssues(): Result<List<GithubIssueDTO>> {
        return Result.Success(emptyList())
    }

    private fun getFailureIssues(): Result<List<GithubIssueDTO>> {
        return Result.Failure(IllegalStateException("KU-KA"))
    }

    private fun generateTestRepo(id: Int): GithubRepositoryDTO = GithubRepositoryDTO(
        id = id,
        name = "${id}My Awesome Repo",
        url = "https://github.com",
        description = LOREM,
        starsCount = 1,
        owner = getGithubUserDTO()
    )

    private fun generateTestIssue(id: Int): GithubIssueDTO = GithubIssueDTO(
        id = id,
        title = "I have a $id question",
        body = LOREM,
        user = getGithubUserDTO()
    )

    private fun getGithubUserDTO(): GithubOwnerDTO = GithubOwnerDTO(
        name = "Vasiliy Pupkin",
        id = 1,
        login = "QUQ",
        url = "asd",
        avatar = "https://vk.com/sticker/1-56771-512"
    )
}