package com.gitlab.prichman.testdrom.data.repository

import com.gitlab.prichman.testdrom.core.data.Result
import com.gitlab.prichman.testdrom.data.datasource.NetworkDataSource
import com.gitlab.prichman.testdrom.data.vo.GithubIssueDTO
import com.gitlab.prichman.testdrom.domain.data.GithubOwner
import com.gitlab.prichman.testdrom.domain.data.GithubRepository
import com.gitlab.prichman.testdrom.domain.repository.GithubRepo
import com.gitlab.prichman.testdrom.data.vo.GithubRepositoryDTO
import com.gitlab.prichman.testdrom.domain.data.GithubIssue


class GithubRepoImpl(
    private val networkDataSource: NetworkDataSource
) : GithubRepo {

    override suspend fun getAllReposForQuery(
        query: String,
        perPage: Int,
        page: Int
    ): Result<List<GithubRepository>> {
        val result = networkDataSource.searchRepositories(
            query = query,
            perPage = perPage,
            page = page
        )
        if (result is Result.Failure) return result

        val reposDto = (result as Result.Success).value
        return Result.Success(value = reposDto.map { repoFromDto(it) })
    }

    override suspend fun getIssues(
        owner: String,
        repo: String,
        perPage: Int,
        page: Int
    ): Result<List<GithubIssue>> {
        val result = networkDataSource.getIssues(
            owner = owner,
            repo = repo,
            perPage = perPage,
            page = page
        )
        if (result is Result.Failure) return result

        val issuesDto = (result as Result.Success).value
        return Result.Success(value = issuesDto.map { issueFromDto(it) })
    }

    private fun repoFromDto(dto: GithubRepositoryDTO): GithubRepository = GithubRepository(
        id = dto.id,
        name = dto.name,
        url = dto.url,
        description = dto.description,
        starsCount = dto.starsCount,
        owner = GithubOwner(
            id = dto.id,
            name = dto.owner.name ?: dto.owner.login,
            avatarUrl = dto.owner.avatar
        )
    )

    private fun issueFromDto(dto: GithubIssueDTO): GithubIssue = GithubIssue(
        id = dto.id,
        title = dto.title,
        body = dto.body ?: "",
        user = GithubOwner(
            id = dto.id,
            name = dto.user.name ?: dto.user.login,
            avatarUrl = dto.user.avatar
        )
    )
}