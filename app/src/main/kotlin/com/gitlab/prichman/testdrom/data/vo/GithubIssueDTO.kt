package com.gitlab.prichman.testdrom.data.vo

data class GithubIssueDTO(
    val id: Int,
    val title: String,
    val body: String?,
    val user: GithubOwnerDTO
)