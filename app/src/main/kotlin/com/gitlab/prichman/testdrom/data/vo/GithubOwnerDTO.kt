package com.gitlab.prichman.testdrom.data.vo

import com.google.gson.annotations.SerializedName

data class GithubOwnerDTO(
    val id: Int,
    val login: String,
    val name: String?,
    val url: String,

    @SerializedName("avatar_url")
    val avatar: String
)