package com.gitlab.prichman.testdrom.data.vo

import com.google.gson.annotations.SerializedName

data class GithubRepositoryDTO(
    val id: Int,
    val name: String,
    val url: String,
    val description: String?,

    @SerializedName("stargazers_count")
    val starsCount: Int,

    @SerializedName("owner")
    val owner: GithubOwnerDTO
)