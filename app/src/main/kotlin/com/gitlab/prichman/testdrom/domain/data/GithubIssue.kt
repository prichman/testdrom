package com.gitlab.prichman.testdrom.domain.data

class GithubIssue(
    val id: Int,
    val title: String,
    val body: String,
    val user: GithubOwner
)