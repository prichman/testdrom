package com.gitlab.prichman.testdrom.domain.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
class GithubOwner(
    val id: Int,
    val name: String,
    val avatarUrl: String
) : Parcelable