package com.gitlab.prichman.testdrom.domain.data

import android.os.Parcelable

import kotlinx.parcelize.Parcelize

@Parcelize
data class GithubRepository(
    val id: Int,
    val name: String,
    val url: String,
    val description: String?,
    val starsCount: Int,
    val owner: GithubOwner
) : Parcelable