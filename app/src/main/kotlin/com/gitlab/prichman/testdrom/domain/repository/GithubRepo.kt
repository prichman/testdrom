package com.gitlab.prichman.testdrom.domain.repository

import com.gitlab.prichman.testdrom.core.data.Result
import com.gitlab.prichman.testdrom.domain.data.GithubIssue
import com.gitlab.prichman.testdrom.domain.data.GithubRepository

interface GithubRepo {
    suspend fun getAllReposForQuery(
        query: String,
        perPage: Int,
        page: Int
    ): Result<List<GithubRepository>>

    suspend fun getIssues(
        owner: String,
        repo: String,
        perPage: Int,
        page: Int
    ): Result<List<GithubIssue>>
}