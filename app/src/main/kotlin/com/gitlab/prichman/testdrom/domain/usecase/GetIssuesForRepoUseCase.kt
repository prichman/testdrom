package com.gitlab.prichman.testdrom.domain.usecase

import com.gitlab.prichman.testdrom.domain.data.GithubIssue
import com.gitlab.prichman.testdrom.presentation.data.UiResult


interface GetIssuesForRepoUseCase {
    suspend operator fun invoke(
        owner: String,
        repo: String,
        perPage: Int,
        page: Int
    ): UiResult<List<GithubIssue>>
}