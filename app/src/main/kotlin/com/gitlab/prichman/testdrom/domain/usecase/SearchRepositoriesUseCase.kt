package com.gitlab.prichman.testdrom.domain.usecase

import com.gitlab.prichman.testdrom.domain.data.GithubRepository
import com.gitlab.prichman.testdrom.presentation.data.UiResult


interface SearchRepositoriesUseCase {
    suspend operator fun invoke(
        query: String,
        perPage: Int,
        page: Int
    ): UiResult<List<GithubRepository>>
}