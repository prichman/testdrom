package com.gitlab.prichman.testdrom.domain.usecase.impl

import com.gitlab.prichman.testdrom.core.data.Result
import com.gitlab.prichman.testdrom.core.domain.ExceptionHandler
import com.gitlab.prichman.testdrom.domain.data.GithubIssue
import com.gitlab.prichman.testdrom.domain.repository.GithubRepo
import com.gitlab.prichman.testdrom.domain.usecase.GetIssuesForRepoUseCase
import com.gitlab.prichman.testdrom.presentation.data.UiResult


class GetIssuesForRepoUseCaseImpl(
    private val githubRepo: GithubRepo,
    private val exceptionHandler: ExceptionHandler
) : GetIssuesForRepoUseCase {

    override suspend fun invoke(
        owner: String,
        repo: String,
        perPage: Int,
        page: Int
    ): UiResult<List<GithubIssue>> {
        val result = githubRepo.getIssues(owner, repo, perPage, page)
        return if (result is Result.Failure) {
            val failureReason = exceptionHandler.handle(result.error)
            UiResult.Failure(failureReason)
        } else {
            UiResult.Success((result as Result.Success).value)
        }
    }
}