package com.gitlab.prichman.testdrom.domain.usecase.impl

import com.gitlab.prichman.testdrom.core.data.Result
import com.gitlab.prichman.testdrom.core.domain.ExceptionHandler

import com.gitlab.prichman.testdrom.domain.data.GithubRepository
import com.gitlab.prichman.testdrom.domain.repository.GithubRepo
import com.gitlab.prichman.testdrom.domain.usecase.SearchRepositoriesUseCase
import com.gitlab.prichman.testdrom.presentation.data.UiResult


class SearchRepositoriesUseCaseImpl(
    private val repo: GithubRepo,
    private val exceptionHandler: ExceptionHandler
) : SearchRepositoriesUseCase {

    override suspend fun invoke(query: String, perPage: Int, page: Int): UiResult<List<GithubRepository>> {
        val result = repo.getAllReposForQuery(query, perPage, page)
        return if (result is Result.Failure) {
            val failureReason = exceptionHandler.handle(result.error)
            UiResult.Failure(failureReason)
        } else {
            UiResult.Success((result as Result.Success).value)
        }
    }
}