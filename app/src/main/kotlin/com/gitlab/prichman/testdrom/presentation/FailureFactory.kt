package com.gitlab.prichman.testdrom.presentation

import com.gitlab.prichman.testdrom.core.NetworkException
import com.gitlab.prichman.testdrom.core.NoConnectionException
import com.gitlab.prichman.testdrom.core.RequestQuotaExceedException
import com.gitlab.prichman.testdrom.core.ResourceManager
import com.gitlab.prichman.testdrom.core.domain.ExceptionHandler
import com.gitlab.prichman.testdrom.core.domain.FailureReason
import com.gitlab.prichman.testdrom.presentation.data.GeneralError
import com.gitlab.prichman.testdrom.presentation.data.NetworkError
import com.gitlab.prichman.testdrom.presentation.data.NoConnection
import com.gitlab.prichman.testdrom.presentation.data.TooManyRequests


class FailureFactory(private val resourceManager: ResourceManager) : ExceptionHandler {
    override fun handle(e: Exception): FailureReason  = when (e) {
        is NoConnectionException -> NoConnection(resourceManager)
        is NetworkException -> NetworkError(resourceManager)
        is RequestQuotaExceedException -> TooManyRequests(resourceManager)
        else -> GeneralError(resourceManager)
    }

}