package com.gitlab.prichman.testdrom.presentation

import android.content.Context

import androidx.annotation.StringRes

import com.gitlab.prichman.testdrom.core.ResourceManager


class ResourceManagerImpl(private val context: Context) : ResourceManager {

    override fun getString(@StringRes stringResId: Int) = context.getString(stringResId)
}