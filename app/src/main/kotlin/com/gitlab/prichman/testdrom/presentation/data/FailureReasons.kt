package com.gitlab.prichman.testdrom.presentation.data

import com.gitlab.prichman.testdrom.R
import com.gitlab.prichman.testdrom.core.ResourceManager
import com.gitlab.prichman.testdrom.core.domain.FailureReason


abstract class AbstractFailureReason(private val resourceManager: ResourceManager) : FailureReason {

    abstract fun messageResId(): Int

    override val message: String
        get() = resourceManager.getString(messageResId())
}

class NoConnection(resourceManager: ResourceManager) : AbstractFailureReason(resourceManager) {
    override fun messageResId(): Int = R.string.error_no_connection
}

class TooManyRequests(resourceManager: ResourceManager) : AbstractFailureReason(resourceManager) {
    override fun messageResId(): Int = R.string.error_quota_exceeded
}

class NetworkError(resourceManager: ResourceManager) : AbstractFailureReason(resourceManager) {
    override fun messageResId(): Int = R.string.error_network
}

class GeneralError(resourceManager: ResourceManager) : AbstractFailureReason(resourceManager) {
    override fun messageResId(): Int = R.string.error_general
}