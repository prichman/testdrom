package com.gitlab.prichman.testdrom.presentation.data

import com.gitlab.prichman.testdrom.core.domain.FailureReason


sealed class UiResult<out T> {
    class Success<out R>(val value: R) : UiResult<R>()
    class Failure(val reason: FailureReason) : UiResult<Nothing>()
}
