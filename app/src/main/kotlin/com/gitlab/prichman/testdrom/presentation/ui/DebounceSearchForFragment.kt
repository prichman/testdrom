package com.gitlab.prichman.testdrom.presentation.ui

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.coroutineScope
import androidx.lifecycle.repeatOnLifecycle

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch


class DebounceSearchForFragment : WithDebounceSearch {

    private companion object {
        private const val DELAY = 500L
    }

    private val _searchFlow: MutableStateFlow<String> = MutableStateFlow("")

    override val searchFlow: StateFlow<String> = _searchFlow

    override fun onNewSearch(query: String) {
        _searchFlow.value = query
    }

    override fun subscribeToSearch(
        lifecycle: Lifecycle,
        onCollect: (String) -> Unit
    ) {
        val scope = lifecycle.coroutineScope
        scope.launch(scope.coroutineContext + Job()) {
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                searchFlow.debounce(DELAY)
                    .filterNot { it.isEmpty() }
                    .flowOn(Dispatchers.IO)
                    .collect {
                        onCollect(it)
                    }
            }
        }
    }
}