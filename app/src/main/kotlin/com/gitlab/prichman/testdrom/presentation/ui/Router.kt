package com.gitlab.prichman.testdrom.presentation.ui

import android.os.Bundle

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit


object Router {

    private lateinit var fragmentManager: FragmentManager
    private var containerId: Int = 0

    fun registerFragmentManager(manager: FragmentManager, containerId: Int) {
        fragmentManager = manager
        Router.containerId = containerId
    }

    fun navigate(fragment: Fragment) {
        fragmentManager.commit {
            setReorderingAllowed(true)
            replace(containerId, fragment)
            addToBackStack(null)
        }
    }

    fun navigate(fragment: Fragment, params: Bundle) {
        fragment.arguments = Bundle().apply {
            putAll(params)
        }
        navigate(fragment)
    }
}