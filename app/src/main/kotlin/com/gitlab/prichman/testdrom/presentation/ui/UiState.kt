package com.gitlab.prichman.testdrom.presentation.ui

import com.gitlab.prichman.testdrom.core.domain.FailureReason

interface WithData<out T : Any>{
    val data: List<T>
    val currentPage: Int
}

interface WithError {
    val reason: FailureReason
}

sealed class UiState<out T : Any> {
    object Initial : UiState<Nothing>()
    object EmptyLoading : UiState<Nothing>()

    class Loading : UiState<Nothing>()
    class Refresh : UiState<Nothing>()

    class EmptyError(
        override val reason: FailureReason
    ) : UiState<Nothing>(), WithError

    class Error<out T : Any>(
        override val reason: FailureReason,
        override val data: List<T>,
        override val currentPage: Int
    ) : UiState<T>(), WithError, WithData<T>

    class Data<out T : Any>(
        override val data: List<T>,
        override val currentPage: Int
    ) : UiState<T>(), WithData<T>

    class FullData<out T : Any>(
        override val data: List<T>,
        override val currentPage: Int
    ) : UiState<T>(), WithData<T>
}