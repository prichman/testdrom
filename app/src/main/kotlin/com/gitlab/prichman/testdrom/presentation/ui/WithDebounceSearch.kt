package com.gitlab.prichman.testdrom.presentation.ui

import androidx.lifecycle.Lifecycle

import kotlinx.coroutines.flow.StateFlow


interface WithDebounceSearch {
    val searchFlow: StateFlow<String>

    fun onNewSearch(query: String)

    fun subscribeToSearch(
        lifecycle: Lifecycle,
        onCollect: (String) -> Unit
    )
}