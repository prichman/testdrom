package com.gitlab.prichman.testdrom.presentation.ui.activity

import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity

import com.gitlab.prichman.testdrom.R
import com.gitlab.prichman.testdrom.presentation.ui.Router


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Router.registerFragmentManager(supportFragmentManager, R.id.fragmentContainerView)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}