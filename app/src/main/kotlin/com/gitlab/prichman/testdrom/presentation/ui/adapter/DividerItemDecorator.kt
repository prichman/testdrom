package com.gitlab.prichman.testdrom.presentation.ui.adapter

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.util.Log

import androidx.recyclerview.widget.RecyclerView
import kotlin.math.roundToInt


class DividerItemDecorator(context: Context) : RecyclerView.ItemDecoration() {

    private val mBounds = Rect()
    private lateinit var divider: Drawable

    init {
        val a: TypedArray = context.obtainStyledAttributes(intArrayOf(android.R.attr.listDivider))
        val dr = a.getDrawable(0)
        if (dr == null) {
            Log.w(
                "DividerItemDecorator",
                "@android:attr/listDivider was not set in the theme used for this "
                        + "DividerItemDecoration. Please set that attribute all call setDrawable()"
            )
        } else {
            divider = dr
        }
        a.recycle()
    }

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        if (parent.layoutManager == null) return

        c.save()
        val left: Int
        val right: Int

        if (parent.clipToPadding) {
            left = parent.paddingLeft
            right = parent.width - parent.paddingRight
            c.clipRect(
                left, parent.paddingTop, right,
                parent.height - parent.paddingBottom
            )
        } else {
            left = 0
            right = parent.width
        }

        val childCount = parent.childCount
        for (i in 0 until childCount - 1) {
            val child = parent.getChildAt(i)
            parent.getDecoratedBoundsWithMargins(child, mBounds)
            val bottom: Int = mBounds.bottom + child.translationY.roundToInt()
            val top: Int = bottom - divider.intrinsicHeight
            divider.setBounds(left, top, right, bottom)
            divider.draw(c)
        }
        c.restore()
    }
}