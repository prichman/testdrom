package com.gitlab.prichman.testdrom.presentation.ui.adapter

import android.annotation.SuppressLint

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

import com.gitlab.prichman.testdrom.presentation.ui.adapter.delegate.ProgressAdapterDelegate
import com.gitlab.prichman.testdrom.presentation.ui.adapter.delegate.ProgressItem

import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter


class PagingAdapter(
    private val getNextPage: () -> Unit,
    private val itemDiff: (old: Any, new: Any) -> Boolean,
    vararg delegates: AdapterDelegate<MutableList<Any>>
) : AsyncListDifferDelegationAdapter<Any>(
        object : DiffUtil.ItemCallback<Any>() {
            override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean {
                if (oldItem === newItem) return true
                return itemDiff.invoke(oldItem, newItem)
            }

            override fun getChangePayload(oldItem: Any, newItem: Any) = Any()

            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean = oldItem == newItem
        }
) {
    private companion object {
        private const val BEFORE_LAST_ITEM = 5
    }

    private var fullData = false

    init {
        items = mutableListOf()
        delegatesManager.addDelegate(ProgressAdapterDelegate())
        delegates.forEach { delegatesManager.addDelegate(it) }

        stateRestorationPolicy = StateRestorationPolicy.PREVENT_WHEN_EMPTY
    }

    fun update(data: List<Any>, isPageProgress: Boolean) {
        items = mutableListOf<Any>().apply {
            addAll(data)
            if (isPageProgress) add(ProgressItem)
        }
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int,
        payloads: MutableList<Any?>
    ) {
        super.onBindViewHolder(holder, position, payloads)

        if (!fullData && position >= items.size - BEFORE_LAST_ITEM) getNextPage.invoke()
    }
}
