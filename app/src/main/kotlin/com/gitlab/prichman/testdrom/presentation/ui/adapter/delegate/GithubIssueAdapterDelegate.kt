package com.gitlab.prichman.testdrom.presentation.ui.adapter.delegate

import android.view.View
import android.view.ViewGroup

import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView

import com.bumptech.glide.RequestManager

import com.gitlab.prichman.testdrom.R
import com.gitlab.prichman.testdrom.core.presentation.ui.UiUtils
import com.gitlab.prichman.testdrom.core.presentation.ui.inflate
import com.gitlab.prichman.testdrom.domain.data.GithubIssue

import com.hannesdorfmann.adapterdelegates4.AdapterDelegate


class GithubIssueAdapterDelegate(
    private val glide: RequestManager
) : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int): Boolean =
        items[position] is GithubIssue

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val v = parent.inflate(R.layout.issue_item_layout)
        return GithubIssueVH(v)
    }

    override fun onBindViewHolder(
        items: MutableList<Any>,
        position: Int,
        holder: RecyclerView.ViewHolder,
        payloads: MutableList<Any>
    ) {
        val vh = holder as GithubIssueVH
        val issue = items[position] as GithubIssue
        vh.bind(issue)
    }

    private inner class GithubIssueVH(
        v: View
    ) : RecyclerView.ViewHolder(v) {

        private val avatarView: AppCompatImageView = v.findViewById(R.id.avatar)
        private val repoTitle: AppCompatTextView = v.findViewById(R.id.repo_name)
        private val repoDescr: AppCompatTextView = v.findViewById(R.id.repo_description)

        fun bind(
            issue: GithubIssue
        ) {
            UiUtils.loadGlideAvatar(glide, issue.user.avatarUrl, avatarView)

            repoTitle.text = issue.title
            repoDescr.text = issue.body
        }
    }
}