package com.gitlab.prichman.testdrom.presentation.ui.adapter.delegate

import android.view.View
import android.view.ViewGroup

import androidx.recyclerview.widget.RecyclerView

import com.gitlab.prichman.testdrom.R
import com.gitlab.prichman.testdrom.core.presentation.ui.inflate

import com.hannesdorfmann.adapterdelegates4.AdapterDelegate


class ProgressAdapterDelegate : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int) =
        items[position] is ProgressItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        ViewHolder(parent.inflate(R.layout.item_progress))

    override fun onBindViewHolder(
        items: MutableList<Any>,
        position: Int,
        viewHolder: RecyclerView.ViewHolder,
        payloads: MutableList<Any>
    ) {}

    private class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}
