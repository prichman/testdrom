package com.gitlab.prichman.testdrom.presentation.ui.adapter.delegate

import android.view.View
import android.view.ViewGroup

import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView

import com.bumptech.glide.RequestManager

import com.gitlab.prichman.testdrom.R
import com.gitlab.prichman.testdrom.core.presentation.ui.UiUtils
import com.gitlab.prichman.testdrom.core.presentation.ui.inflate
import com.gitlab.prichman.testdrom.domain.data.GithubRepository

import com.hannesdorfmann.adapterdelegates4.AdapterDelegate


class SearchRepositoryAdapterDelegate(
    private val clickListener: (GithubRepository) -> Unit,
    private val glide: RequestManager
) : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int): Boolean =
        items[position] is GithubRepository

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val v = parent.inflate(R.layout.search_repository_view_layout)
        return GithubRepoVH(v)
    }

    override fun onBindViewHolder(
        items: MutableList<Any>,
        position: Int,
        holder: RecyclerView.ViewHolder,
        payloads: MutableList<Any>
    ) {
        val vh = holder as GithubRepoVH
        val repo = items[position] as GithubRepository
        vh.bind(repo, clickListener)
    }

    private inner class GithubRepoVH(
        v: View
    ) : RecyclerView.ViewHolder(v) {

        private val avatarView: AppCompatImageView = v.findViewById(R.id.avatar)
        private val repoTitle: AppCompatTextView = v.findViewById(R.id.repo_name)
        private val repoDescr: AppCompatTextView = v.findViewById(R.id.repo_description)

        fun bind(
            repo: GithubRepository,
            itemClickListener: (GithubRepository) -> Unit
        ) {
            UiUtils.loadGlideAvatar(glide, repo.owner.avatarUrl, avatarView)

            repoTitle.text = repo.name
            repoDescr.text = repo.description

            itemView.setOnClickListener{
                itemClickListener(repo)
            }
        }
    }
}