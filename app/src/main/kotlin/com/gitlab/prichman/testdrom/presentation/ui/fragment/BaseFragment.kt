package com.gitlab.prichman.testdrom.presentation.ui.fragment

import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

open class BaseFragment : Fragment {

    constructor() : super()
    constructor(@LayoutRes layoutId: Int) : super(layoutId)

    fun setBackButtonEnable(enable: Boolean) {
        (activity as? AppCompatActivity)?.apply {
            supportActionBar?.setDisplayHomeAsUpEnabled(enable)
            supportActionBar?.setDisplayShowHomeEnabled(enable)
        }
    }
}