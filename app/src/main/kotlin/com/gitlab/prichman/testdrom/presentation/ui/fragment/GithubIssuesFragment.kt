package com.gitlab.prichman.testdrom.presentation.ui.fragment

import android.os.Bundle
import android.view.View

import androidx.lifecycle.*

import com.bumptech.glide.Glide

import com.gitlab.prichman.testdrom.R
import com.gitlab.prichman.testdrom.app.App
import com.gitlab.prichman.testdrom.domain.data.GithubIssue
import com.gitlab.prichman.testdrom.domain.data.GithubRepository
import com.gitlab.prichman.testdrom.presentation.ui.UiState
import com.gitlab.prichman.testdrom.presentation.ui.adapter.PagingAdapter
import com.gitlab.prichman.testdrom.presentation.ui.adapter.delegate.GithubIssueAdapterDelegate
import com.gitlab.prichman.testdrom.presentation.ui.view.FullRepositoryView
import com.gitlab.prichman.testdrom.presentation.ui.view.PagingView
import com.gitlab.prichman.testdrom.presentation.viewmodel.GithubIssuesViewModel

import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


class GithubIssuesFragment : BaseFragment(R.layout.github_issues_fragment) {

    companion object {
        const val KEY_PARAM_REPO = "repo"

        fun newInstance() = GithubIssuesFragment()
    }

    private val glide by lazy { Glide.with(this) }

    private val adapter by lazy { PagingAdapter(
        getNextPage = { },
        itemDiff = { old, new ->
            if (old is GithubIssue && new is GithubIssue) {
                old.id == new.id
            } else {
                false
            }
        },
        GithubIssueAdapterDelegate(glide)
    ) }

    private lateinit var viewModel: GithubIssuesViewModel

    // UI elements
    private lateinit var pagingView: PagingView
    private lateinit var repoView: FullRepositoryView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        viewModel = ViewModelProvider(this, App.app.viewModelsFactory)
            .get(GithubIssuesViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindUi(view)
        setBackButtonEnable(true)
        readArguments(savedInstanceState != null)
        initSubscribers()
    }

    private fun readArguments(savedState: Boolean) {
        arguments?.let { args ->
            val repo = args.getParcelable<GithubRepository>(KEY_PARAM_REPO)!!


            repoView.setup(
                repo.owner.avatarUrl,
                repo.owner.name,
                repo.name,
                repo.description ?: ""
            )

            if (!savedState) {
                viewModel.init(repo)
                viewModel.load()
            }
        }
    }

    private fun bindUi(view: View) {
        repoView = view.findViewById(R.id.repo_view)
        pagingView = view.findViewById(R.id.paging_view)
        pagingView.init(adapter) {
            viewModel.refresh()
        }
    }

    private fun initSubscribers() {
        viewLifecycleOwner.lifecycleScope.launch(viewLifecycleOwner.lifecycle.coroutineScope.coroutineContext + Job()) {
            viewLifecycleOwner.lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.uiState.collect { state ->
                    handleIssueState(state)
                }
            }
        }
    }

    private fun handleIssueState(state: UiState<GithubIssue>) {
        pagingView.handleState(state)
    }
}