package com.gitlab.prichman.testdrom.presentation.ui.fragment

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.*
import android.view.inputmethod.EditorInfo

import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle

import com.bumptech.glide.Glide

import com.gitlab.prichman.testdrom.R
import com.gitlab.prichman.testdrom.app.App
import com.gitlab.prichman.testdrom.domain.data.GithubRepository
import com.gitlab.prichman.testdrom.presentation.ui.DebounceSearchForFragment
import com.gitlab.prichman.testdrom.presentation.ui.Router
import com.gitlab.prichman.testdrom.presentation.ui.UiState
import com.gitlab.prichman.testdrom.presentation.ui.WithDebounceSearch
import com.gitlab.prichman.testdrom.presentation.ui.adapter.PagingAdapter
import com.gitlab.prichman.testdrom.presentation.ui.adapter.delegate.SearchRepositoryAdapterDelegate
import com.gitlab.prichman.testdrom.presentation.ui.view.PagingView
import com.gitlab.prichman.testdrom.presentation.viewmodel.SearchRepositoriesViewModel

import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch


class SearchRepositoriesFragment : BaseFragment(R.layout.search_fragment) {

    private companion object {
        private const val KEY_SEARCH_QUERY      = "search_query"
        private const val KEY_SEARCH_EXPANDED   = "search_expanded"
    }

    private val adapter by lazy { PagingAdapter(
        getNextPage = { viewModel.loadMore() },
        itemDiff = { old, new ->
            if (old is GithubRepository && new is GithubRepository) {
                old.id == new.id
            } else {
                false
            }
        },
        SearchRepositoryAdapterDelegate(
            clickListener = { repo ->
                Router.navigate(GithubIssuesFragment.newInstance(), Bundle().apply {
                    putParcelable(GithubIssuesFragment.KEY_PARAM_REPO, repo)
                })
            },
            glide = Glide.with(this)
        )
    ) }

    private lateinit var viewModel: SearchRepositoriesViewModel

    // UI elements
    private lateinit var pagingView: PagingView
    private lateinit var searchView: SearchView

    // Restore items
    private var searchQuery: String = ""
    private var searchExpanded: Boolean = false

    private val debounceSearch: WithDebounceSearch = DebounceSearchForFragment()

    private val queryTextListener = object : SearchView.OnQueryTextListener {

        private var prevSearch = ""

        override fun onQueryTextSubmit(query: String?): Boolean = true

        override fun onQueryTextChange(newText: String?): Boolean {
            if (prevSearch.isEmpty() && newText.isNullOrEmpty()) return true

            prevSearch = newText ?: ""
            debounceSearch.onNewSearch(prevSearch)

            return true
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        viewModel = ViewModelProvider(this, App.app.viewModelsFactory)
            .get(SearchRepositoriesViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        prepareRestoredState(savedInstanceState)
        bindUi(view)
        setBackButtonEnable(false)
        initSubscribers()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_fragment_menu, menu)

        val searchManager = requireContext().getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView.apply {
            imeOptions = imeOptions or EditorInfo.IME_FLAG_NO_EXTRACT_UI
            setSearchableInfo(searchManager.getSearchableInfo(requireActivity().componentName))

            // RestorePrevState
            isIconified = !searchExpanded
            maxWidth = Integer.MAX_VALUE
            setQuery(searchQuery, true)
            if (searchQuery.isNotBlank()) {
                // Case when search and rotate device before debounce is done
                debounceSearch.onNewSearch(searchQuery)
            }

            setOnQueryTextListener(queryTextListener)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        if (item.itemId == R.id.action_search) {
            true
        } else {
            super.onOptionsItemSelected(item)
        }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        if (!this::searchView.isInitialized) return

        val searchString = searchView.query.toString()
        val isExpanded = !searchView.isIconified

        outState.putString(KEY_SEARCH_QUERY, searchString)
        outState.putBoolean(KEY_SEARCH_EXPANDED, isExpanded)
    }

    private fun prepareRestoredState(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) return

        searchQuery    = savedInstanceState.getString(KEY_SEARCH_QUERY, "")
        searchExpanded = savedInstanceState.getBoolean(KEY_SEARCH_EXPANDED, false)
    }

    private fun bindUi(view: View) {
        pagingView = view.findViewById(R.id.paging_view)
        pagingView.init(adapter, true) {
            viewModel.refresh()
        }
    }

    private fun initSubscribers() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.uiState.collect { state ->
                    handleSearchState(state)
                }
            }
        }

        debounceSearch.subscribeToSearch(viewLifecycleOwner.lifecycle) { searchString ->
            viewModel.load(searchString)
        }
    }

    private fun handleSearchState(state: UiState<GithubRepository>) {
        pagingView.handleState(state)
    }
}