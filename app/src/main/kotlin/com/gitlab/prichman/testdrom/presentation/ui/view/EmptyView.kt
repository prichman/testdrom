package com.gitlab.prichman.testdrom.presentation.ui.view

import android.content.Context
import android.util.AttributeSet
import android.widget.Button
import androidx.appcompat.widget.AppCompatTextView

import androidx.constraintlayout.widget.ConstraintLayout

import com.gitlab.prichman.testdrom.R
import com.gitlab.prichman.testdrom.core.presentation.ui.hide
import com.gitlab.prichman.testdrom.core.presentation.ui.inflate
import com.gitlab.prichman.testdrom.core.presentation.ui.show


class EmptyView : ConstraintLayout {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    )

    private val refreshButton: Button
    private val textView: AppCompatTextView

    init {
        inflate(R.layout.view_empty, true)

        textView = findViewById(R.id.text_view)
        refreshButton = findViewById(R.id.refresh_button)
    }

    fun setRefreshListener(onClick: () -> Unit) {
        refreshButton.setOnClickListener { onClick.invoke() }
    }

    fun showInitial(prompt: Boolean) {
        refreshButton.hide()

        if (prompt) {
            textView.text = context.getString(R.string.empty_view_hello)
            show()
        } else {
            hide()
        }
    }

    fun showEmptyData() {
        refreshButton.hide()

        textView.text = context.getString(R.string.empty_view_no_data)
        show()
    }

    fun showEmptyError(msg: String) {
        refreshButton.show()

        textView.text = msg
        show()
    }
}