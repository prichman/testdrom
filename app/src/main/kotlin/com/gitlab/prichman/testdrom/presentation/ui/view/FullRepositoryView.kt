package com.gitlab.prichman.testdrom.presentation.ui.view

import android.content.Context
import android.util.AttributeSet

import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout

import com.bumptech.glide.Glide

import com.gitlab.prichman.testdrom.R
import com.gitlab.prichman.testdrom.core.presentation.ui.inflate


class FullRepositoryView : ConstraintLayout {

    private val avatarView: AppCompatImageView
    private val ownerNameView: AppCompatTextView
    private val repoTitleView: AppCompatTextView
    private val repoDescrView: AppCompatTextView

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    )

    init {
        inflate(R.layout.view_full_repository, true)

        avatarView = findViewById(R.id.user_avatar)
        ownerNameView = findViewById(R.id.owner_name)
        repoTitleView = findViewById(R.id.repository_title)
        repoDescrView = findViewById(R.id.repository_description)
    }

    fun setup(
        avatarUrl: String,
        ownerName: String,
        repoTitle: String,
        repoDescr: String
    ) {
        Glide.with(this)
            .asDrawable()
            .load(avatarUrl)
            .centerCrop()
            .circleCrop()
            .into(avatarView)

        ownerNameView.text = ownerName
        repoTitleView.text = repoTitle
        repoDescrView.text = repoDescr
    }
}