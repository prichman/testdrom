package com.gitlab.prichman.testdrom.presentation.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.Toast

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

import com.gitlab.prichman.testdrom.R
import com.gitlab.prichman.testdrom.core.presentation.ui.hide
import com.gitlab.prichman.testdrom.presentation.ui.adapter.PagingAdapter
import com.gitlab.prichman.testdrom.core.presentation.ui.inflate
import com.gitlab.prichman.testdrom.core.presentation.ui.show
import com.gitlab.prichman.testdrom.presentation.ui.UiState
import com.gitlab.prichman.testdrom.presentation.ui.adapter.DividerItemDecorator


class PagingView : FrameLayout {

    private var showPrompt: Boolean = false
    private var onRefresh: (() -> Unit)? = null

    private val refreshLayout: SwipeRefreshLayout
    private val rv: RecyclerView
    private val emptyView: EmptyView
    private val fullscreenProgressView: View

    private lateinit var adapter: PagingAdapter

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    )

    init {
        inflate(R.layout.view_paging_list, true)

        refreshLayout           = findViewById(R.id.refresh_layout)
        rv                      = findViewById(R.id.rv)
        emptyView               = findViewById(R.id.empty_view)
        fullscreenProgressView  = findViewById(R.id.fullscreen_progress_view)

        val layoutManager = LinearLayoutManager(context)
        rv.layoutManager = layoutManager

        rv.addItemDecoration(DividerItemDecorator(context))
    }

    fun init(
        adapter: PagingAdapter,
        showPrompt: Boolean = false,
        onRefresh: () -> Unit
    ) {
        this.adapter = adapter
        this.onRefresh = onRefresh
        this.showPrompt = showPrompt

        rv.adapter = adapter

        emptyView.setRefreshListener(onRefresh)
        refreshLayout.setOnRefreshListener { onRefresh() }
    }

    fun handleState(state: UiState<*>) {
        when (state) {
            UiState.Initial -> {
                emptyView.showInitial(showPrompt)
                fullscreenProgressView.hide()
                refreshLayout.isRefreshing = false
                refreshLayout.hide()

                adapter.update(emptyList(), false)
            }
            UiState.EmptyLoading -> {
                emptyView.hide()
                refreshLayout.hide()
                refreshLayout.isRefreshing = false
                fullscreenProgressView.show()
            }
            is UiState.Loading -> {
                emptyView.hide()
                fullscreenProgressView.hide()
                refreshLayout.isRefreshing = false
                refreshLayout.show()

                adapter.update(adapter.items ?: emptyList(), true)
            }
            is UiState.Refresh -> {
                emptyView.hide()
                fullscreenProgressView.hide()
                refreshLayout.isRefreshing = true
                refreshLayout.show()

                adapter.update(adapter.items ?: emptyList(), false)
            }
            is UiState.EmptyError -> {
                fullscreenProgressView.hide()
                refreshLayout.isRefreshing = false
                refreshLayout.hide()
                emptyView.showEmptyError(state.reason.message)
            }
            is UiState.Error -> {
                fullscreenProgressView.hide()
                refreshLayout.isRefreshing = false
                refreshLayout.show()
                emptyView.hide()

                adapter.update(state.data, false)

                Toast.makeText(context, state.reason.message, Toast.LENGTH_SHORT).show()
            }
            is UiState.Data -> {
                fullscreenProgressView.hide()
                emptyView.hide()
                refreshLayout.isRefreshing = false
                refreshLayout.show()

                adapter.update(state.data, false)
            }
            is UiState.FullData -> {
                fullscreenProgressView.hide()
                refreshLayout.isRefreshing = false
                if (state.data.isEmpty()) {
                    emptyView.showEmptyData()
                    refreshLayout.hide()
                } else {
                    emptyView.hide()
                    refreshLayout.show()
                }
                adapter.update(state.data, false)
            }
        }
    }
}