package com.gitlab.prichman.testdrom.presentation.viewmodel

import com.gitlab.prichman.testdrom.domain.data.GithubIssue
import com.gitlab.prichman.testdrom.domain.data.GithubRepository
import com.gitlab.prichman.testdrom.domain.usecase.GetIssuesForRepoUseCase
import com.gitlab.prichman.testdrom.presentation.data.UiResult


class GithubIssuesViewModel(
    private val getIssues: GetIssuesForRepoUseCase
) : LoadMoreRefreshViewModel<GithubIssue>() {

    private companion object {
        private const val PER_PAGE = 30
    }

    private lateinit var repository: GithubRepository

    fun init(repo: GithubRepository) {
        this.repository = repo
    }

    override suspend fun getLoadResult(): UiResult<List<GithubIssue>> = getIssues(
        owner = repository.owner.name,
        repo = repository.name,
        perPage = PER_PAGE,
        page = 1
    )

    override suspend fun getNextPage(lastPage: Int): UiResult<List<GithubIssue>> = getLoadResult()
}