package com.gitlab.prichman.testdrom.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope

import com.gitlab.prichman.testdrom.core.domain.FailureReason

import com.gitlab.prichman.testdrom.presentation.data.UiResult
import com.gitlab.prichman.testdrom.presentation.ui.UiState
import com.gitlab.prichman.testdrom.presentation.ui.WithData

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch


abstract class LoadMoreRefreshViewModel<T : Any> : ViewModel(), StatefulListPaginator {

    private val _uiState: MutableStateFlow<UiState<T>> = MutableStateFlow(UiState.Initial)
    val uiState: StateFlow<UiState<T>> = _uiState

    abstract suspend fun getLoadResult(): UiResult<List<T>>
    abstract suspend fun getNextPage(lastPage: Int): UiResult<List<T>>

    override fun load() {
        viewModelScope.launch {
            val prevState = _uiState.value
            val prevStateWasEmpty = prevState is UiState.Initial ||
                    (prevState as? WithData<*>)?.data.isNullOrEmpty()

            _uiState.value = getLoadingState(prevStateWasEmpty)

            var result: UiResult<List<T>> = getLoadResult()
            if (result is UiResult.Failure) {
                _uiState.value = getErrorState(
                    prevStateWasEmpty || prevState is UiState.EmptyError,
                    result.reason,
                    prevState as? WithData<T>,
                )
                return@launch
            }

            result = result as UiResult.Success

            val newItems = result.value
            _uiState.value =  if (newItems.isEmpty()) {
                UiState.FullData(newItems, 1)
            } else {
                UiState.Data(newItems, 1)
            }
        }
    }

    override fun loadMore() {
        viewModelScope.launch {
            val prevState = _uiState.value
            if (prevState !is UiState.Data &&
                prevState !is UiState.Error) {
                return@launch
            }

            _uiState.value = getLoadingState(false)

            val lastPage = (prevState as WithData<*>).currentPage

            var result: UiResult<List<T>> = getNextPage(lastPage)
            val prevWithData = prevState as WithData<T>
            if (result is UiResult.Failure) {
                _uiState.value = getErrorState(
                    prevState is UiState.EmptyError,
                    result.reason,
                    prevWithData
                )
                return@launch
            }

            result = result as UiResult.Success

            val newRepos = result.value
            val oldRepos = prevWithData.data
            _uiState.value =  if (newRepos.isEmpty()) {
                UiState.FullData(newRepos, lastPage)
            } else {
                UiState.Data(oldRepos + newRepos, lastPage + 1)
            }
        }
    }

    override fun refresh() {
        viewModelScope.launch {
            val prevState = _uiState.value
            if (prevState is UiState.Loading) return@launch

            _uiState.value = UiState.Refresh()

            var result: UiResult<List<T>> = getLoadResult()
            val prevWithData = prevState as? WithData<T>
            if (result is UiResult.Failure) {
                _uiState.value = getErrorState(
                    prevState is UiState.EmptyError,
                    result.reason,
                    prevWithData
                )
                return@launch
            }

            result = result as UiResult.Success

            val newRepos = result.value
            _uiState.value =  if (newRepos.isEmpty()) {
                UiState.FullData(newRepos, 1)
            } else {
                UiState.Data(newRepos, 1)
            }
        }
    }

    private fun getLoadingState(prevStateWasEmpty: Boolean): UiState<T> = if (prevStateWasEmpty) {
        UiState.EmptyLoading
    } else {
        UiState.Loading()
    }

    private fun getErrorState(
        prevStateWasEmpty: Boolean,
        reason: FailureReason,
        withData: WithData<T>? = null
    ): UiState<T> = if (prevStateWasEmpty || withData == null) {
        UiState.EmptyError(reason)
    } else {
        UiState.Error(reason, withData.data, withData.currentPage)
    }
}