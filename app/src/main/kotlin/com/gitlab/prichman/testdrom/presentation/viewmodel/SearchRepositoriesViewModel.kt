package com.gitlab.prichman.testdrom.presentation.viewmodel

import com.gitlab.prichman.testdrom.domain.data.GithubRepository
import com.gitlab.prichman.testdrom.domain.usecase.SearchRepositoriesUseCase
import com.gitlab.prichman.testdrom.presentation.data.UiResult


class SearchRepositoriesViewModel(
    private val searchRepos: SearchRepositoriesUseCase
) : LoadMoreRefreshViewModel<GithubRepository>() {

    private companion object {
        private const val PER_PAGE = 30
    }

    private var lastQuery: String = ""

    override suspend fun getLoadResult(): UiResult<List<GithubRepository>> =
        searchRepos(lastQuery, PER_PAGE, 1)

    override suspend fun getNextPage(lastPage: Int): UiResult<List<GithubRepository>> =
        searchRepos(lastQuery, PER_PAGE, lastPage + 1)

    fun load(query: String) {
        lastQuery = query

        load()
    }
}