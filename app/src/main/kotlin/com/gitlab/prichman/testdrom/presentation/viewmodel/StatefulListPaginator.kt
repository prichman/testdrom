package com.gitlab.prichman.testdrom.presentation.viewmodel

interface StatefulListPaginator {
    fun load()
    fun loadMore()
    fun refresh()
}