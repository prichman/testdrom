package com.gitlab.prichman.testdrom.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

import com.gitlab.prichman.testdrom.domain.usecase.GetIssuesForRepoUseCase
import com.gitlab.prichman.testdrom.domain.usecase.SearchRepositoriesUseCase


class ViewModelsFactory(
    private val searchRepositoriesUseCase: SearchRepositoriesUseCase,
    private val getIssuesUseCase: GetIssuesForRepoUseCase,
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SearchRepositoriesViewModel::class.java)) {
            return SearchRepositoriesViewModel(searchRepositoriesUseCase) as T
        }
        if (modelClass.isAssignableFrom(GithubIssuesViewModel::class.java)) {
            return GithubIssuesViewModel(
                getIssues = getIssuesUseCase
            ) as T
        }

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}